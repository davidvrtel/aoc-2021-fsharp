#load "misc.fs";;

open Miscellaneous.Functions.Common
open Miscellaneous.Functions.Day12

// Data loading
// ------------------

let data = readLinesToSeq @"input\day12.txt"

let parsedData =
    data
    |> Seq.map (fun x ->
            x.Split [|'-'|] |> Set.ofArray
        )

let neighbourMap =
    parsedData
    |> Array.map



// Part 1
// ------------------
let mapPaths start ends tunnels=

    let mutable paths = [||]

    starts
    |> Seq.map (fun x ->
            tunnels
            |> Seq.map (fun x ->
                    if
                )
    )

// result1 |> printfn "Part 1: %d"

// Part 2
// ------------------

// result2 |> printfn "Part 2: %d"
