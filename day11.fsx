#load "misc.fs";;

open Miscellaneous.Functions.Common
open Miscellaneous.Functions.Day11

// Data loading
// ------------------
let data = readLinesToSeq @"input\day11.txt"

let parsedData = array2D data |> Array2D.map (string >> int)

// Part 1
// ------------------
let result1 = raiseEnergyLevel parsedData 0 1 Part1

result1 |> printfn "Part 1: %d"

// Part 2
// ------------------
let result2 = raiseEnergyLevel parsedData 0 1 Part2

result2 |> printfn "Part 2: %d"
