#load "misc.fs";;

open Miscellaneous.Functions.Common
open Miscellaneous.Functions.Day7

// Data loading
// ------------------
let data = readLinesToSeq @"input\day07.txt" |> Seq.head

let parsedData = data.Split [|','|] |> List.ofArray |> List.map int

// Part 1_1
// ------------------
let (_, minFuel) = parsedData |> fuelPerPos calculateFuel1 |> Array.minBy (fun (v, a) -> a)
minFuel |> printfn "Part 1_1: %d"

// Part 1_2
// ------------------
// Solution utilizing fact that for min sum of distances (linear regression aprox.) can be used median
let histogram =
    parsedData
    |> List.sort

let optimalPos = histogram[histogram.Length/2]
let result1_2 = (0, parsedData) ||> List.fold(fun acc x -> acc + (abs (optimalPos-x)))
result1_2 |> printfn "Part 1_2: %d"

// Part 2
// ------------------
let (_, minFuel2) = parsedData |> fuelPerPos calculateFuel2 |> Array.minBy (fun (v, a) -> a)
minFuel2 |> printfn "Part 2: %d"

// result2 |> printfn "Part 2: %d"
