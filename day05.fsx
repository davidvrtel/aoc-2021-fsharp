#load "misc.fs";;

open Miscellaneous.Functions.Common
open Miscellaneous.Functions.Day5

// Data loading
// ------------------
let data = readLinesToSeq @"input\day05.txt"

let parsedData =
    data
    |> Seq.toList
    |> List.map (fun x ->
            x.Split([|" -> "|], System.StringSplitOptions.None)
            |> Array.map (fun x -> x.Split [|','|] |> Array.map int)
        )

let nonDiagonalLines =
    parsedData
    |> List.filter (fun vec -> if vec[0][0] = vec[1][0] || vec[0][1] = vec[1][1] then true else false )

let diagonalLines =
    parsedData
    |> List.filter (fun vec ->
            if abs (vec[0][0]-vec[1][0]) = abs (vec[0][1]-vec[1][1]) then
                true
            else
                false
        )

let oceanMap: int[,] = Array2D.zeroCreate 1000 1000

// Part 1
// ------------------
nonDiagonalLines
|> List.iteri (fun i vec ->
        markNonDiagonalLines oceanMap vec
    )

let result1 = countIntersections oceanMap

result1 |> printfn "Part 1: %d"

// Part 2
// ------------------
diagonalLines
|> List.iteri (fun i vec ->
        markDiagonalLines oceanMap vec
    )

let result2 = countIntersections oceanMap

result2 |> printfn "Part 2: %d"
// result2 |> printfn "Part 2: %d"

// |> printfn "Part 2: %d"



// arrays with same coordinates on second position fail

// let problematic = nonDiagonalLines[1]
// let parsed = markNonDiagonalLines oceanMap problematic
// let length = nonDiagonalLines.Length
