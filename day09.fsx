#load "misc.fs";;

open Miscellaneous.Functions.Common
open Miscellaneous.Functions.Day9

// Data loading
// ------------------
let data = readLinesToSeq @"input\day09.txt"

let parsedData = array2D data |> Array2D.map (string >> int)

// Part 1
// ------------------
let lowPoints = getLowPoints parsedData

let result1 = lowPoints |>  Seq.sumBy (fun (x, y, v) -> v+1 )

result1 |> printfn "Part 1: %d"

// Part 2
// ------------------
let lowPoints2 = getLowPoints parsedData |> Seq.toArray

let basinSizes =
    lowPoints2
    |> Array.map (fun (x, y, v) ->
            let map = mapWave [|(x, y)|] [||] parsedData
            map.Length
        )
    |> Array.sortDescending

let result2 = basinSizes[0] * basinSizes[1] * basinSizes[2]

result2 |> printfn "Part 2: %d"
