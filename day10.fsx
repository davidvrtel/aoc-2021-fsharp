#load "misc.fs";;

open Miscellaneous.Functions.Common
open Miscellaneous.Functions.Day10

// Data loading
// ------------------
let data = readLinesToSeq @"input\day10.txt"

// let parsedData = array2D data |> Array2D.map (string >> int)
let parsedData = data |> Seq.map (fun x -> Seq.toArray x) |> Seq.toArray

let testData ="""
[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]
"""



let parsedTestData = testData.Split [|'\n'|] |> Seq.map (fun x -> Seq.toArray x) |> Seq.toArray

// Part 1
// ------------------

let illegalChars = genIllegalChars parsedData

let result1 =
    (0, illegalChars)
    ||> Array.fold (fun acc (x, closing) ->
            let addition =
                match x with
                | ')' -> 3
                | ']' -> 57
                | '}' -> 1197
                | '>' -> 25137
                | _ -> 0
            acc + addition
        )

result1 |> printfn "Part 1: %d"

// Part 2
// ------------------
let incomplete = illegalChars |> Array.filter (fun (x, closing) -> x = ' ')
let result2 =
    incomplete
    |> Array.map (fun (x, closing) ->
            (closing, uint64 0)
            ||> Array.foldBack (fun y acc ->
                    let addition =
                        match y with
                        | ')' -> uint64 1
                        | ']' -> uint64 2
                        | '}' -> uint64 3
                        | '>' -> uint64 4
                        | _ -> uint64 0
                    (uint64 5)*acc + addition
                )
        )
    |> Array.sort


result2[result2.Length/2] |> printfn "Part 2: %d"


