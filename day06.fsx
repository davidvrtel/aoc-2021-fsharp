#load "misc.fs";;

open Miscellaneous.Functions.Common
open Miscellaneous.Functions.Day6

// Data loading
// ------------------
let data = readLinesToSeq @"input\day06.txt" |> Seq.head
let parsedData =  data.Split [|','|] |> Array.map int |> List.ofArray

// for second part, counts how many fish has certain timer which results in easier computation and less iterations
let timer: uint64[] = Array.create 9 (uint64 0)

let prepareTimer parsedData=
    parsedData
    |> List.iter (fun x ->
            timer[x] <- timer[x]+ (uint64 1)
        )

// Part 1
// ------------------
let fishAfter80Days = advanceOneDay parsedData 1 80

let result1 = List.length fishAfter80Days
result1 |> printfn "Part 1: %d"

// Part 2_1
// ------------------
prepareTimer parsedData
adjustTimer timer 1 256

let result2_1 = Array.sum timer
result2_1 |> printfn "Part 2_1: %d"

// Part 2_2
// ------------------
let initialTimer =
    (Array.create 9 (uint64 0), parsedData)
    ||> List.fold (fun acc fishTimer ->
            acc
            |> Array.mapi (fun i x ->
                    if i = fishTimer then x + (uint64 1) else x
                )
        )

let fishAfter256Days = adjustTimer2 initialTimer 1 256

let result2_2 = Array.sum fishAfter256Days
result2_2 |> printfn "Part 2_2: %d"
