# Advent of Code 2021 in F#
All AoC 2021 days and parts done in F#.

Run with `dotnet fsi dayXX.fsx`. It's dependant on input being in `input` directory and using some modules with `.fs` extension.
