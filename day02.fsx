#load "misc.fs";;

open Miscellaneous.Functions.Common
open Miscellaneous.Functions.Day2

// Data loading
// ------------------
let data = readLinesToSeq @"input\day02.txt"

// Part 1
// ------------------
((0,0), parseDirections data) ||> Seq.fold (fun (accH, accV) directions->
        match directions with
        | ("forward", x) -> (accH + x, accV)
        | ("up", x) -> (accH, accV - x)
        | ("down", x) -> (accH, accV + x)
        | _ -> (accH, accV)
    )
|> fun (x, y) -> x * y
|> printfn "Part 1: %d"

// Part 2
// ------------------
((0, 0, 0), parseDirections data) ||> Seq.fold (fun (accH, accV, accA) directions->
        match directions with
        | ("forward", x) -> (accH + x, accV + accA * x, accA)
        | ("up", x) -> (accH, accV, accA - x)
        | ("down", x) -> (accH, accV, accA + x)
        | _ -> (accH, accV, accA)
    )
|> fun (x, y, _) -> x * y
|> printfn "Part 2: %d"
