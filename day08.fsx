#load "misc.fs";;

open Miscellaneous.Functions.Common
open Miscellaneous.Functions.Day8

// Data loading
// ------------------
let data = readLinesToSeq @"input\day08.txt"

let parsedData =
    data
    |> Seq.map (fun x ->
            let inputLine = x.Split([|'|'|], System.StringSplitOptions.TrimEntries)
            let splitNums = inputLine[0].Split([|' '|], System.StringSplitOptions.TrimEntries)
            let splitOutput = inputLine[1].Split([|' '|], System.StringSplitOptions.TrimEntries)
            (seq splitNums, seq splitOutput)
        )

let parsedDigits =
    parsedData
    |> Seq.collect (fun x -> fst x)

let parsedOutput =
    parsedData
    |> Seq.collect (fun x -> snd x)

let parsedData2 = decodeInput data

// Part 1
// ------------------
let result1 =
    (0, parsedOutput)
    ||> Seq.fold (fun acc x ->
            match x.Length with
            | 2 | 3 | 4 | 7 -> acc+1
            | _ -> acc
        )

result1 |> printfn "Part 1: %d"

// Part 2
// ------------------
let result2 =
    (0, parsedData2)
    ||> Seq.fold (fun acc (digits, numbers) ->
            let segmentMap = getSegmentMap digits

            let result =
                ("", numbers)
                ||> Seq.fold (fun num x ->
                        num + string (getNumber segmentMap x)
                    )
                |> int

            acc + result
        )

result2 |> printfn "Part 2: %d"
