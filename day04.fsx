#load "misc.fs";;

open Miscellaneous.Functions.Common
open Miscellaneous.Functions.Day4

// Data loading
// ------------------
let data = readLinesToSeq @"input\day04.txt"

let drawnNums = (Seq.head data).Split [|','|] |> Array.map int

let parsedData =
    Seq.tail data
    |> Seq.toArray
    |> Array.filter (fun x -> x <> "")
    |> Array.chunkBySize 5
    |> Array.map (fun x ->
        x
        |> Array.map (fun y ->
        y.Split [|' '|]
            |> Array.filter (fun y -> y <> "")
            |> Array.map int
        )
    )
    |> Array.map array2D

/// Array keeps track of all occurrences for given index in dimension of 5x5 array
/// Initialized as array of following structure [|[|0; 0; 0; 0; 0|]; [|0; 0; 0; 0; 0|]|]
/// Each array of 5 zeros represents dimension of board with same index as the structure
let boardWinningMap =
    Array.init parsedData.Length (fun _ ->
        Array.init 2 (fun _ ->
            Array.create 5 0
        )
    )

let boardLoosingMap =
    Array.init parsedData.Length (fun _ ->
        Array.init 2 (fun _ ->
            Array.create 5 0
        )
    )

// Part 1
// ------------------
let (num, numIndex, index, dimension, position) = getFirstWinningBoard parsedData 0 boardWinningMap drawnNums

let result = (sumUnmarked parsedData[index] numIndex drawnNums) * num

result |> printfn "Part 1: %d"

// Part 2
// ------------------

let (num2, numIndex2, index2) = getLastWinningBoard parsedData 0 boardLoosingMap drawnNums (0,0,0)

let result2 = (sumUnmarked parsedData[index2] numIndex2 drawnNums) * num2

result2 |> printfn "Part 2: %d"

// |> printfn "Part 2: %d"
