#load "misc.fs";;

open Miscellaneous.Functions.Common
open Miscellaneous.Functions.Day3

// Data loading
// ------------------
let data = readLinesToSeq @"input\day03.txt"
let dataLength = Seq.length data
let parsedData = convertStrArrToBinArr data

// Part 1
// ------------------
let onesInPos = countOnesInPos parsedData

let gammaBinArr =
    getGammaEpsilon onesInPos dataLength Gamma

let epsilonBinArr =
    getGammaEpsilon onesInPos dataLength Epsilon

(convertBinArrayToInt gammaBinArr) * (convertBinArrayToInt epsilonBinArr) |> printfn "Part 1: %d"

// Part 2
// ------------------

let o2 = reduceToLifeSupportRate 0 parsedData O2Generator |> Array.exactlyOne |> convertBinArrayToInt
printfn "--------------------------------------------"
let co2 = reduceToLifeSupportRate 0 parsedData CO2Scrubber |> Array.exactlyOne |> convertBinArrayToInt

o2 * co2 |> printfn "Part 2: %d"
