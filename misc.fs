module Miscellaneous

module Functions =
    module Common =
    /// Reads contents of specified file and saves each line to sequence
        let readLinesToSeq filePath : seq<string> = 
            System.IO.File.ReadLines(
                    System.IO.Path.GetFullPath(filePath)
                )

    module Day1 =

        /// Function using Seq.scan to generate it triplets similarly to pairwise out of seq of int
        let generateIntTriplets (inputData: seq<int>) =
            inputData
            |> Seq.scan
                (fun (_, previousPrevious, previous) current ->
                    (previousPrevious, previous, current)
                )
                (0, 0, 0)
            |> Seq.skip 3

        /// Takes in seq of int and compares all pairs from start to finish.
        /// Returns of all occurrences where first item of pair is smaller than second item.
        let countPairComparisons dataSeq =
            dataSeq
            |> Seq.pairwise
            |> Seq.map (fun (x, y) -> if x < y then 1 else 0)
            |> Seq.sum

    module Day2 =

        /// Get direction data with each line in format "direction units", return sequence of tuples with direction first and units second
        let parseDirections (inputData: seq<string>) =
            inputData
            |> Seq.map (fun x ->
                    match x.Split [|' '|] with
                    | [| direction; units |] -> (direction, int units)
                    | _ -> ("", 0)
                )

    module Day3 =

        type ConsumptionRate =
            | Gamma
            | Epsilon

        type LifeSupportRate =
            | O2Generator
            | CO2Scrubber

        /// Wrapper for Convert.ToInt32
        let convertBinToInt binaryString = int ("0b" + binaryString)

        let convertIntToBinStr (integer: int) = System.Convert.ToString(integer, 2)

        let convertStrArrToBinArr (inputData: seq<string>) =
            inputData
            |> Seq.map (fun charSeq ->
                    charSeq
                    |> Seq.map (fun x -> int x - int '0')
                    |> Seq.toArray
                )
            |> Seq.toArray

        /// Counts ones in input and returns array with count on same position
        let countOnesInPos (inputData: int[][]) =
            inputData
            |> Array.reduce (Array.map2 (fun a r -> a+r))

        ///
        let getGammaEpsilon inputData dataLength switcher =
            inputData
            |> Array.map (fun x ->
                    System.Convert.ToInt32(
                        match switcher with
                        | Gamma -> x > dataLength/2
                        | Epsilon -> x< dataLength/2
                    )
                )

        let convertIntArrayToBinStr (inputData: int[]) =
            inputData
            |> Array.map string
            |> String.concat ""

        let convertBinArrayToInt inputData =
            inputData
            |> convertIntArrayToBinStr
            |> convertBinToInt

        /// Recursivelly go through the input data
        /// Sum all the ones on position and filter out data based on conditions
        /// Filtered data are passed as input data to next iteration
        /// Iterating ends when only one value is left
        let rec reduceToLifeSupportRate (index: int) (inputData: int[][]) rateType =
            let dataLengthM = (float inputData.Length) / 2.0

            let countInIndex =
                (0, inputData)
                ||> Array.fold (fun acc record ->
                    acc + record[index])
                |> float

            let identifier =
                match rateType with
                | O2Generator ->
                    if countInIndex > dataLengthM then 1 elif countInIndex = dataLengthM then 1 else 0
                | CO2Scrubber ->
                    if countInIndex > dataLengthM then 0 elif countInIndex = dataLengthM then 0 else 1

            let data =
                inputData
                |> Array.filter (fun record -> record[index] = identifier)

            if data.Length > 1 then
                reduceToLifeSupportRate (index+1) data rateType
            else
                data

    module Day4 =

        /// This function recursively counts first winning round of bingo
        /// Utilized board map counting occurrences in each row and column of the board
        /// When there are 5 occurrences the board wins
        let rec getFirstWinningBoard (inputData: int[,][]) numIndex (boardMap: int[][][]) (drawnNums: int[]) =

            let num = drawnNums[numIndex]
            let mutable result = (0, 0, 0, 0, 0) // (num, numIndex, index, dimension, position)

            inputData
            |> Array.iteri (fun index board ->
                board
                |> Array2D.iteri (fun j k v ->
                    if v = num then
                        if boardMap.[index].[0].[j]+1 = 5 then
                            if result = (0,0,0,0,0) then result <- (num, numIndex, index, 0, j)
                        elif boardMap.[index].[1].[k]+1 = 5 then
                            if result = (0,0,0,0,0) then result <- (num, numIndex, index, 1, k)
                        else
                            boardMap.[index].[0].[j] <- boardMap.[index].[0].[j]+1
                            boardMap.[index].[1].[k] <- boardMap.[index].[1].[k]+1
                )
            )


            if numIndex+1 < drawnNums.Length && result = (0,0,0,0,0) then
                getFirstWinningBoard inputData (numIndex+1) boardMap drawnNums
            else
                result

        /// Similar to First Winning Board, but this one uses different condition
        /// result from previous iteration is passed to next which shows last round that had first 5 occurrences
        let rec getLastWinningBoard (inputData: int[,][]) numIndex (boardMap: int[][][]) (drawnNums: int[]) (result: int * int * int) =

            let num = drawnNums[numIndex]
            let mutable result = result // (num, numIndex, index)

            inputData
            |> Array.iteri (fun index board ->
                board
                |> Array2D.iteri (fun j k v ->
                    if v = num then
                        let flatMap =
                            boardMap[index]
                            |> Array.concat
                            |> Array.tryFind (fun x -> x = 5)

                        match flatMap with
                        | Some x -> ()
                        | None ->
                            if boardMap.[index].[0].[j]+1 = 5 || boardMap.[index].[1].[k]+1 = 5 then
                                result <- (num, numIndex, index)

                            boardMap.[index].[0].[j] <- boardMap.[index].[0].[j]+1
                            boardMap.[index].[1].[k] <- boardMap.[index].[1].[k]+1
                )
            )

            if numIndex+1 < drawnNums.Length then
                getLastWinningBoard inputData (numIndex+1) boardMap drawnNums result
            else
                result

        /// Take winning 5x5 board and accumulate unmarked values
        /// Unmarked values are determined by iterating over undrawn numbers and matching them to the board
        let sumUnmarked (array: int[,]) numIndex (drawnNums: int[]) =
            let mutable accumulator = 0

            drawnNums[numIndex+1..]
            |> Array.iter (fun num ->
                array
                |> Array2D.iter (fun x ->
                    if x = num then
                        accumulator <- accumulator+num
                )
            )
            accumulator

    module Day5 =

        /// Function builds on top of option to blit the 2D array.
        /// First determine orientation and size of line and get that part from ocean map.
        /// Second increment every value by one and replace that exact part on the ocean map.
        let markNonDiagonalLines (oceanMap: int[,]) (vec: int[][]) =

            let getVSubMap (oceanMap: int[,]) x y z =
                [|oceanMap[x, y..z]|] |> array2D

            let getHSubMap (oceanMap: int[,]) x y z =
                let tmp = [|oceanMap[x..y, z]|] |> array2D
                Array2D.init (tmp |> Array2D.length2) (tmp |> Array2D.length1) (fun r c -> tmp.[c,r])

            let get0DSubMap (oceanMap: int[,]) x y =
                [|[|oceanMap[x, y]|]|] |> array2D

            let blitMapWithResult (oceanMap: int[,]) x1 x2 y1 y2 (source: int[,])=
                oceanMap[x1..x2, y1..y2] <- source


            if vec[0][0] = vec[1][0] then
                if vec[0][1] > vec[1][1] then
                    let (x, y, z) = (vec[0][0], vec[1][1], vec[0][1])
                    getVSubMap oceanMap x y z
                    |> Array2D.map (fun q -> q+1)
                    |> blitMapWithResult oceanMap x x y z

                else
                    let (x, y, z) = (vec[0][0], vec[0][1], vec[1][1])
                    getVSubMap oceanMap x y z
                    |> Array2D.map (fun q -> q+1)
                    |> blitMapWithResult oceanMap x x y z

            elif vec[0][1] = vec[1][1] then
                if vec[0][0] > vec[1][0] then
                    let (x, y, z) = (vec[1][0], vec[0][0], vec[0][1])
                    getHSubMap oceanMap x y z
                    |> Array2D.map (fun q -> q+1)
                    |> blitMapWithResult oceanMap x y z z
                else
                    let (x, y, z) = (vec[0][0], vec[1][0], vec[0][1])
                    getHSubMap oceanMap x y z
                    |> Array2D.map (fun q -> q+1)
                    |> blitMapWithResult oceanMap x y z z
            else
                let (x, y) = (vec[0][0], vec[0][1])
                get0DSubMap oceanMap x y
                |> Array2D.map (fun q -> q+1)
                |> blitMapWithResult oceanMap x x y y

        /// Similar approach to non-diagonal lines but different in how the blit is done.
        /// Diagonals can be iterated over by one step in each coordinate.
        /// This function calculates side of the diagonal's square and goes one by one incrementing the values.
        let markDiagonalLines (oceanMap: int[,]) (vec: int[][]) =
            let iterations = abs (vec[0][0]-vec[1][0])

            for i = 0 to iterations do

                if vec[0][0] > vec[1][0] then
                    if vec[0][1] > vec[1][1] then
                        oceanMap[vec[0][0]-i, vec[0][1]-i] <- oceanMap[vec[0][0]-i, vec[0][1]-i]+1
                    else
                        oceanMap[vec[0][0]-i, vec[0][1]+i] <- oceanMap[vec[0][0]-i, vec[0][1]+i]+1
                else
                    if vec[0][1] > vec[1][1] then
                        oceanMap[vec[0][0]+i, vec[0][1]-i] <- oceanMap[vec[0][0]+i, vec[0][1]-i]+1
                    else
                        oceanMap[vec[0][0]+i, vec[0][1]+i] <- oceanMap[vec[0][0]+i, vec[0][1]+i]+1

        /// Simple function that takes Array2D and counts each value bigger than 1
        let countIntersections (oceanMap: int[,]) =

            let mutable counter = 0

            oceanMap
            |> Array2D.iter (fun x -> if x > 1 then counter <- counter+1)

            counter

    module Day6 =

        /// Naive approach to calculate results.
        /// Works fine for smaller sets of data (100k records) but gets exponentionally slower
        let rec advanceOneDay
            (inputData: int list)
            (day: int)
            (endDay: int)
            :int list
            =

            let (updatedFish, numOfNewborns) =
                (0, inputData)
                ||> List.mapFold (fun acc fishDay ->
                        if fishDay = 0 then
                            6, acc+1
                        else
                            fishDay-1, acc
                    )

            let newborns = List.replicate numOfNewborns 8
            let newGeneration = List.concat [updatedFish; newborns]

            if day < endDay then
                advanceOneDay newGeneration (day+1) endDay
            else
                newGeneration

        /// Better way of calculating fish in the sea.
        /// Instead of adjusting timer of each fish and adding more fish to the list
        /// this solution counts amount of fish for each timer and shifts them on day progression
        let rec adjustTimer
            (timer: uint64 array)
            (day: int)
            (endDay: int)
            =

            let newborn = Array.head timer // number of newborn is same as number of fish that just reproduced, which are those with timer 0

            // shift timer for each index, values go to index from index+1, only value not updated is index = 8 which gets newborns
            // starts from second element because first timer shifts to index 6 and 8 afterwards
            timer
            |> Array.tail
            |> Array.iteri (fun i x ->
                    timer[i] <- x
                )

            timer[8] <- newborn  // set newborns
            timer[6] <- timer[6]+newborn  // set fish that just reproduced

            if day < endDay then
                adjustTimer timer (day+1) endDay
            else
                ()

        /// Alternative approach, more functional. Function takes reference to only pass data to the next
        /// generation of time that is being passed to next iteration. Data being copied instead of reused.
        let rec adjustTimer2
            (timer: uint64 array)
            (day: int)
            (endDay: int)
            : uint64 array
            =

            let newborn = Array.head timer // number of newborn is same as number of fish that just reproduced, which are those with timer 0

            // shift timer for each index, timer 6 gets + fish that reproduced, timer 8 gets newborn
            let nextTimer: uint64[] =
                Array.create 9 (uint64 0)
                |> Array.mapi (fun i x ->
                        if i = 6
                        then
                            timer[i+1] + newborn
                        elif i = 8
                        then
                            newborn
                        else
                            timer[i+1]
                    )

            if day < endDay then
                adjustTimer2 nextTimer (day+1) endDay
            else
                nextTimer

    module Day7 =

        /// <summary>Function type for folding fuel consumption. Returns folded array of pairs , fst is position, snd is fuel consumption</summary>
        /// <typeparam name="(int * int) array"> initial array of pairs</typeparam>
        /// <typeparam name="int">current position</typeparam>
        /// <returns><c>(int * int) array</c>: fst is position, snd is fuel consumption</returns>
        type FuelCalculator = (int * int) array -> int -> (int * int) array

        /// Fuel consumed is equal to distance (absolute value) from current and target position.
        let calculateFuel1 acc x =
            acc |> Array.map (fun (v, a) -> (v, a + (abs (x-v))))

        /// Fuel consumed is equal to sum of natural numbers starting with 1 and
        /// ending with distance (absolute value) from current and target position.
        let calculateFuel2 acc x =

            let rec sumOfN sum size =
                if size > 0
                then sumOfN (sum + size) (size-1)
                else sum + size

            acc
            |> Array.map (fun (v, a) ->
                    (
                        v,
                        a + (sumOfN 0 (abs (x-v)))
                    )
                )

        /// Bruteforce calculation of each possible position from the input data.
        /// Accumulates result tuple consisting of position and fuel consumed.
        /// distanceMethod is folding method calculateFuel1 or calculateFuel2 for each part
        let fuelPerPos
            (distanceMethod: FuelCalculator)
            (parsedData: list<int>)
            =

            // Initial value for fold is (int * int) array containing position and fuel consumed
            (
                parsedData
                |> Array.ofList
                |> Array.map (fun x -> (x, 0)),
                parsedData
            )
            ||> List.fold distanceMethod

    module Day8 =
        // This solution is so far the most complex and maybe unintentionally harder way

        /// Used to map displayed digits to actuall segment signal
        type SegmentMap = {
            mutable a: char
            mutable b: char
            mutable c: char
            mutable d: char
            mutable e: char
            mutable f: char
            mutable g: char
        }

        /// Parses input to have all digits as strings with characters alphabetically ordered
        /// Returns digits part and four numbers as tuple of sequences of strings
        let decodeInput (inputData: seq<string>) =
            inputData
            |> Seq.map(fun x ->
                    let split = x.Split ([|'|'|], System.StringSplitOptions.TrimEntries)

                    let digits =
                        seq (split[0].Split([|' '|], System.StringSplitOptions.TrimEntries)
                            |> Seq.map (Seq.sort >> System.String.Concat)
                            // |> Seq.map set
                            )

                    let numbers =
                        seq (split[1].Split([|' '|], System.StringSplitOptions.TrimEntries)
                            |> Seq.map (Seq.sort >> System.String.Concat)
                            // |> Seq.map set
                            )
                    (digits, numbers)
                )

        /// Generates segment map with above defined type for later mapping of displayed values
        let getSegmentMap digits =

            let digitSets =
                digits
                |> Seq.map set
                |> Seq.filter (fun x ->
                        if x.Count = 2 || x.Count = 3 || x.Count = 4
                        then true
                        else false
                    )
                |> Seq.sortBy (fun x -> x.Count)
                |> Seq.toArray

            let (one, seven, four) = (digitSets[0], digitSets[1], digitSets[2])

            let segmentCount =
                digits
                |> Seq.collect (fun x -> x)
                |> Seq.countBy (fun x -> x)

            let segmentMap: SegmentMap = {
                a = ' '
                b = ' '
                c = ' '
                d = ' '
                e = ' '
                f = ' '
                g = ' '
            }

            segmentCount
            |> Seq.iter (fun (character, amount) ->
                    match amount with
                    | 4 -> segmentMap.e <- character
                    | 6 -> segmentMap.b <- character
                    | 9 -> segmentMap.f <- character
                    | _ -> ()
                )

            let a = (seven - one) |>  Set.toArray |> Array.exactlyOne
            let c = (one - (Set.empty |> Set.add segmentMap.f)) |> Set.toArray |> Array.exactlyOne
            let d = (four - (Set.add segmentMap.b one)) |>  Set.toArray |> Array.exactlyOne
            let g =
                segmentCount
                |> Seq.pick (fun (character, amount) ->
                        if amount = 7 && character <> d
                        then Some (character)
                        else None
                    )

            segmentMap.a <- a
            segmentMap.c <- c
            segmentMap.d <- d
            segmentMap.g <- g

            segmentMap

        /// Converts number displayed after parsing it with segment map and outputting as string
        let getNumber (m: SegmentMap) number =
            let abcefg  = 0
            let cf      = 1
            let acdeg   = 2
            let acdfg   = 3
            let bcdf    = 4
            let abdfg   = 5
            let abdefg  = 6
            let acf     = 7
            let abcdefg = 8
            let abcdfg  = 9

            let valueMap =
                [|
                    string m.a + string m.b + string m.c + string m.e + string m.f + string m.g
                    string m.c + string m.f
                    string m.a + string m.c + string m.d + string m.e + string m.g
                    string m.a + string m.c + string m.d + string m.f + string m.g
                    string m.b + string m.c + string m.d + string m.f
                    string m.a + string m.b + string m.d + string m.f + string m.g
                    string m.a + string m.b + string m.d + string m.e + string m.f + string m.g
                    string m.a + string m.c + string m.f
                    string m.a + string m.b + string m.c + string m.d + string m.e + string m.f + string m.g
                    string m.a + string m.b + string m.c + string m.d + string m.f + string m.g
                |]


            let displayedNumber =
                valueMap
                |> Array.map (Seq.sort >> System.String.Concat)
                |> Array.findIndex (fun x ->
                        x = number
                    )

            displayedNumber

    module Day9 =

        /// Mapping over all items and comparing their value to all adjacent
        /// All the conditions are controlling if the point is on the edge or not
        /// to make sure index won't run out of bounds
        let getLowPoints (inputData: int[,]) =
            let lenX = Array2D.length1 inputData
            let lenY = Array2D.length2 inputData

            let mappedData =
                inputData
                |> Array2D.mapi (fun x y v ->
                        if x = 0 then // on X being top row
                            if y = 0 then
                                if v < inputData[x+1, y] && v < inputData[x, y+1] then (x, y, v) else (-1,-1,-1)
                            elif y = lenY-1 then
                                if v < inputData[x+1, y] && v < inputData[x, y-1] then (x, y, v) else (-1,-1,-1)
                            else
                                if v < inputData[x+1, y] && v < inputData[x, y-1] && v < inputData[x, y+1] then (x, y, v) else (-1,-1,-1)
                        elif x = lenX-1 then
                            if y = 0 then
                                if v < inputData[x-1, y] && v < inputData[x, y+1] then (x, y, v) else (-1,-1,-1)
                            elif y = lenY-1 then
                                if v < inputData[x-1, y] && v < inputData[x, y-1] then (x, y, v) else (-1,-1,-1)
                            else
                                if v < inputData[x-1, y] && v < inputData[x, y-1] && v < inputData[x, y+1] then (x, y, v) else (-1,-1,-1)
                        elif y = 0 then
                            if x <> 0 || x <> lenX-1 then
                                if v < inputData[x-1, y] && v < inputData[x+1, y] && v < inputData[x, y+1] then (x, y, v) else (-1,-1,-1)
                            else (-1,-1,-1)
                        elif y = lenY-1 then
                            if x <> 0 || x <> lenX-1 then
                                if v < inputData[x-1, y] && v < inputData[x+1, y] && v < inputData[x, y-1] then (x, y, v) else (-1,-1,-1)
                            else (-1,-1,-1)
                        elif v < inputData[x-1, y] && v < inputData[x+1, y] && v < inputData[x, y-1] && v < inputData[x, y+1] then (x, y, v)
                        else (-1,-1,-1)
                    )

            mappedData
            |> Seq.cast<int*int*int> |> Seq.filter (fun x -> x <> (-1,-1,-1))


        /// Custom operator, equivalent to between
        /// Checks if a is in range b to c inclusive
        let inline (>=<) a (b,c) = a >= b && a<= c

        /// Recursive function that iterates over all points mapping the basins in the array.
        /// Array doneMap is holding items that were already counted in. Array inputMap are points to be checked.
        /// InputData are just mapped all points in the 2D array. Returns doneMap which contains all points in the basin.
        let rec mapWave (inputMap: (int * int)[]) (doneMap: (int * int)[]) (inputData: int[,])=

            let newDoneMap = Array.append doneMap inputMap
            let mutable newMap = [||]

            inputMap
            |> Array.iteri (fun acc (x, y) ->
                    if
                        x+1 >=< (0, 99)
                        && not(Array.exists (fun item -> item = (x+1, y)) newDoneMap)
                        && not(Array.exists (fun item -> item = (x+1, y)) newMap)
                        && inputData[x+1, y] < 9
                        then
                            newMap <- Array.append newMap [|(x+1, y)|]

                    if
                        x-1 >=< (0, 99)
                        && not(Array.exists (fun item -> item = (x-1, y)) newDoneMap)
                        && not(Array.exists (fun item -> item = (x-1, y)) newMap)
                        && inputData[x-1, y] < 9
                        then
                            newMap <- Array.append newMap [|(x-1, y)|]

                    if
                        y+1 >=< (0, 99)
                        && not(Array.exists (fun item -> item = (x, y+1)) newDoneMap)
                        && not(Array.exists (fun item -> item = (x, y+1)) newMap)
                        && inputData[x, y+1] < 9
                        then
                            newMap <- Array.append newMap [|(x, y+1)|]

                    if
                        y-1 >=< (0, 99)
                        && not(Array.exists (fun item -> item = (x, y-1)) newDoneMap)
                        && not(Array.exists (fun item -> item = (x, y-1)) newMap)
                        && inputData[x, y-1] < 9
                        then
                            newMap <- Array.append newMap [|(x, y-1)|]
                )

            if newMap.Length > 0
            then mapWave newMap newDoneMap inputData
            else newDoneMap

    module Day10 =

        /// Accumulates expected closing characters and on iterating over one checks if the last expected character matches.
        /// If not sets unmatched variable to that character and returns bot unmatched and all expected closing characters
        let genIllegalChars parsedData =
            parsedData
            |> Array.map (fun x ->
                    let mutable closing = Array.empty

                    let mutable unmatched = ' '

                    x
                    |> Array.iter (fun y ->
                            if unmatched = ' '
                            then
                                if y = ')' || y = ']' || y = '}' || y = '>'
                                then
                                    if y <> (Array.last closing)
                                    then unmatched <- y
                                    else closing <- (Array.take ((Array.length closing)-1) closing)
                                else
                                    match y with
                                    | '(' -> closing <- (Array.append closing [| ')' |])
                                    | '[' -> closing <- (Array.append closing [| ']' |])
                                    | '{' -> closing <- (Array.append closing [| '}' |])
                                    | '<' -> closing <- (Array.append closing [| '>' |])
                                    | _ -> ()
                        )
                    (unmatched, closing)
                )

    module Day11 =

        type Part =
            | Part1
            | Part2

        let rec raiseEnergyLevel inputData accumulator steps part=
            let newState =
                inputData
                |> Array2D.map (fun x -> x + 1)

            let mutable finished = false
            while not finished do
                newState
                |> Array2D.iteri (fun x y v ->
                        if v > 9
                        then
                            if x-1 >= 0 && newState[x-1,y] <> 0
                            then newState[x-1,y] <- newState[x-1,y]+1

                            if x-1 >= 0 && y+1 <= 9 && newState[x-1,y+1] <> 0
                            then newState[x-1,y+1] <- newState[x-1,y+1]+1

                            if y+1 <= 9 && newState[x,y+1] <> 0
                            then newState[x,y+1] <- newState[x,y+1]+1

                            if x+1 <= 9 && y+1 <= 9 && newState[x+1,y+1] <> 0
                            then newState[x+1,y+1] <- newState[x+1,y+1]+1

                            if x+1 <= 9 && newState[x+1,y] <> 0
                            then newState[x+1,y] <- newState[x+1,y]+1

                            if x+1 <= 9 && y-1 >= 0 && newState[x+1,y-1] <> 0
                            then newState[x+1,y-1] <- newState[x+1,y-1]+1

                            if y-1 >= 0 && newState[x,y-1] <> 0
                            then newState[x,y-1] <- newState[x,y-1]+1

                            if x-1 >= 0 && y-1 >= 0 && newState[x-1,y-1] <> 0
                            then newState[x-1,y-1] <- newState[x-1,y-1]+1

                            newState[x,y] <- 0
                        elif v = 0
                        then
                            newState[x,y] <- 0
                        else
                            newState[x,y] <- v
                    )

                finished <- not(newState |> Seq.cast<int> |> Seq.exists (fun x -> x > 9))

            let flashes = newState |> Seq.cast<int> |> Seq.filter (fun x -> x = 0) |> Seq.length

            let newAccumulator = accumulator + flashes

            match part with
            | Part1 ->
                if steps < 100
                then raiseEnergyLevel newState newAccumulator (steps+1) part
                else newAccumulator
            | Part2 ->
                if flashes <> 100
                then raiseEnergyLevel newState newAccumulator (steps+1) part
                else steps

    module Day12 =
        let stuff = "is"
