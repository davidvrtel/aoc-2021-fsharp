#load "misc.fs";;

open Miscellaneous.Functions.Common
open Miscellaneous.Functions.Day1

// Data loading
// ------------------
let data = readLinesToSeq "input/day01.txt" |> Seq.map int

// Part 1
// ------------------
countPairComparisons data
|> printfn "Part 1: %d"

// Part 2
// ------------------
generateIntTriplets data
|> Seq.map (fun (x, y, z) -> x + y + z)
|> countPairComparisons
|> printfn "Part 2: %d"
